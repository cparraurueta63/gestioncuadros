
CREATE TABLE productos (
  id int NOT NULL AUTO_INCREMENT,
  nombre varchar(255) NOT NULL,
  alto decimal(8,2) DEFAULT NULL,
  ancho decimal(8,2) DEFAULT NULL,
  stock decimal(8,2) DEFAULT NULL,
  valor_normal decimal(10,2) NOT NULL,
  valor_minimo decimal(10,2) NOT NULL,
  valor_intermediario decimal(10,2) NOT NULL,
  estimado_costo decimal(12,2) DEFAULT NULL,
  fecha_creacion timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

CREATE TABLE carrito_temporal (
  id int NOT NULL AUTO_INCREMENT,
  id_producto int DEFAULT NULL,
  cantidad float NOT NULL,
  valor_venta float NOT NULL,
  subtotal float NOT NULL,
  fecha_creacion timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  KEY fk_carrito_temporal_producto (id_producto),
  CONSTRAINT fk_carrito_temporal_producto FOREIGN KEY (id_producto) REFERENCES productos (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

CREATE TABLE categoria_gastos (
  id int NOT NULL AUTO_INCREMENT,
  nombre varchar(255) NOT NULL,
  fecha_creacion timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

CREATE TABLE clientes (
  id int NOT NULL AUTO_INCREMENT,
  cedula varchar(500) CHARACTER SET utf8mb4  DEFAULT NULL,
  nombre varchar(255) CHARACTER SET utf8mb4  DEFAULT NULL,
  celular varchar(10) DEFAULT NULL,
  direccion varchar(200) DEFAULT NULL,
  correo varchar(250) DEFAULT NULL,
  fecha_creacion timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

CREATE TABLE contabilidad (
  id int NOT NULL AUTO_INCREMENT,
  titulo varchar(255) NOT NULL,
  valor float DEFAULT NULL,
  fecha_creacion timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE cuentas_bancarias (
  id int NOT NULL AUTO_INCREMENT,
  nombre_cuenta varchar(50) NOT NULL,
  numero_cuenta varchar(20) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

CREATE TABLE ventas (
  id int NOT NULL AUTO_INCREMENT,
  id_cliente int DEFAULT NULL,
  valor_venta float DEFAULT NULL,
  fecha_creacion timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  saldo_pendiente int DEFAULT NULL,
  estado int DEFAULT NULL,
  tipo_venta int DEFAULT NULL,
  id_cuenta int DEFAULT NULL,
  PRIMARY KEY (id),
  KEY id_cliente (id_cliente),
  KEY id_cuenta_bancaria (id_cuenta),
  CONSTRAINT fk_id_cuentta_bancaria FOREIGN KEY (id_cuenta) REFERENCES cuentas_bancarias (id),
  CONSTRAINT fk_venta_cliente FOREIGN KEY (id_cliente) REFERENCES clientes (id)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4;

CREATE TABLE detalles_venta (
  id int NOT NULL AUTO_INCREMENT,
  id_venta int DEFAULT NULL,
  id_producto int DEFAULT NULL,
  cantidad float DEFAULT NULL,
  subtotal float DEFAULT NULL,
  total float DEFAULT NULL,
  fecha_creacion timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  KEY id_venta (id_venta),
  KEY fk_detalle_venta_producto (id_producto),
  CONSTRAINT fk_detalle_venta_producto FOREIGN KEY (id_producto) REFERENCES productos (id),
  CONSTRAINT fk_detalle_venta_venta FOREIGN KEY (id_venta) REFERENCES ventas (id)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4;

CREATE TABLE gastos (
  id int NOT NULL AUTO_INCREMENT,
  id_categoria int DEFAULT NULL,
  nombre varchar(255) NOT NULL,
  valor float DEFAULT NULL,
  fecha_creacion timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  KEY id_categoria (id_categoria),
  CONSTRAINT gastos_ibfk_1 FOREIGN KEY (id_categoria) REFERENCES categoria_gastos (id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

CREATE TABLE parametros (
  id int NOT NULL AUTO_INCREMENT,
  titulo varchar(255) DEFAULT NULL,
  valor float DEFAULT NULL,
  fecha_creacion timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

CREATE TABLE tipo_venta (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

ALTER TABLE ventas
ADD COLUMN tipo_venta_id INT,
ADD CONSTRAINT fk_tipo_venta FOREIGN KEY (tipo_venta_id) REFERENCES tipo_venta(id);



INSERT INTO `parametros` (`id`, `titulo`, `valor`, `fecha_creacion`) VALUES
	(1, 'Puntos de cálculos cuadros', 14, '2024-03-07 00:54:57'),
	(2, 'Porcentaje de ganancia Normal', 134, '2024-03-07 00:54:57'),
	(3, 'Porcentaje de ganancia Mínimo', 80, '2024-03-07 00:54:57'),
	(4, 'Porcentaje de ganancia Intermediario', 50, '2024-03-07 00:54:57');

  INSERT INTO `cuentas_bancarias` (`id`, `nombre_cuenta`, `numero_cuenta`) VALUES
	(1, 'Cuenta Principal', '1234567890'),
	(2, 'Otra Cuenta', '9876543210'),
	(3, 'Efectivo', '1');

  INSERT INTO `categoria_gastos` (`id`, `nombre`, `fecha_creacion`) VALUES
	(1, 'Insumo', '2024-03-06 19:56:57'),
	(2, 'Herramientas', '2024-03-06 20:50:27'),
	(3, 'Personal', '2024-03-06 20:50:37'),
	(4, 'Viaticos', '2024-03-06 20:52:18');

  INSERT INTO `tipo_venta` (`id`, `nombre`) VALUES
	(1, 'CREDITO'),
	(2, 'CONTADO');