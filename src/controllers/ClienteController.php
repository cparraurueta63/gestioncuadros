<?php


class ClienteController
{
    public function __construct()
    {
        require_once 'src/models/Cliente.php';
    }


    public function validarCampos($nombre, $celular, $direccion, $correo, $cedula)
    {

        $validacionExitosa = true;
        //validar que vengan todos los campos 
        if (empty($nombre) || empty($celular) || empty($direccion) || empty($correo) || empty($cedula)) {
            echo "<script>alert('Todos los campos son obligatorios')</script>";
            $validacionExitosa = false;
            $this->mostrarCliente();
        }
        // Valida el maximo de 50 caracteres del nombre
        if (strlen($nombre) > 50) {
            echo "<script>alert('El nombre no puede tener mas de 50 caracteres')</script>";
            $validacionExitosa = false;
            $this->mostrarCliente();
        }
        // Valida el maximo de 100 caracteres la direccion
        if (strlen($direccion) > 100) {
            echo "<script>alert('La direccion no puede tener mas de 100 caracteres')</script>";
            $validacionExitosa = false;
            $this->mostrarCliente();
        }
        // Valida que el correo sea correcto y no supere los 100 caracteres
        if (!filter_var($correo, FILTER_VALIDATE_EMAIL) || strlen($correo) > 100) {
            echo "<script>alert('Favor ingresar un correo valido')</script>";
            $validacionExitosa = false;
            $this->mostrarCliente();
        }
        // Valida que la cedula tenga maximo 10 caracteres y sea numerico
        if (!is_numeric($cedula) || strlen($cedula) > 10) {
            echo "<script>alert('Favor ingresar una cédula válida')</script>";
            $validacionExitosa = false;
            $this->mostrarCliente();
        }
        // Valida que el campo sea numeric, tenga exactamente 10 dígitos y empiece por 3 
        if (!is_numeric($celular) || strlen($celular) != 10  || !preg_match("/^3/", $celular)) {
            echo "<script>alert('Ingrese un celular válido')</script>";
            $validacionExitosa = false;
            $this->mostrarCliente();
        }

        return $validacionExitosa;
    }





    public function GuardarCliente()
    {
        $clienteModelo = new Cliente();
        $nombre = $_POST['nombre'];
        $celular = $_POST['celular'];
        $direccion = $_POST['direccion'];
        $correo = $_POST['correo'];
        $cedula = $_POST['cedula'];
        $validacionExitosa = true;

        $validacionExitosa = $this->validarCampos($nombre, $celular, $direccion, $correo, $cedula);

        if ($validacionExitosa) {
            $resultado = $clienteModelo->insertarCliente($nombre, $celular, $direccion, $cedula, $correo);
            if ($resultado) {
                $this->mostrarCliente();
                echo "<script>alert('Cliente ingresado con éxito')</script>";
            } else {
                echo "<script>alert('Error en el registro')</script>";
            }
        } else {
            $this->mostrarCliente(); // Mostrar el formulario nuevamente si alguna validación falla
        }
    }


    public function mostrarCliente()
    {
        $clienteModelo = new Cliente();
        $clientes = $clienteModelo->ConsultarCliente();

        require_once "public/view/clientes.php";
    }

    public function VistaEliminarCliente($id)
    {
        $clienteModelo = new Cliente();
        $cliente = $clienteModelo->ConsultarClienteId($id);

        require_once "public/view/layout/cliente/eliminarCliente.php";
    }
    public function EliminarCliente()
    {
        $clienteModelo = new Cliente();
        $id = $_POST['id'];
        $clienteEliminado = $clienteModelo->EliminarCliente($id);
        if ($clienteEliminado) {
            $this->mostrarCliente();
            echo "<script>alert('OK')</script>";
        } else {
            echo "<script>alert('ERROR')</script>";
        }
    }
}
