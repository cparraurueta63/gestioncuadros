<?php

class VentaController
{

    public function __construct()
    {
        require_once 'src/models/Venta.php';
        require_once 'src/models/Cliente.php';
        require_once 'src/models/Producto.php';
        require_once 'src/models/Carrito.php';
    }

    public function mostrarVentas()
    {
        $clientes = $this->mostrarClientes();
        $productos = $this->mostrarProductos();
        $carrito = $this->mostrarCarrito();
        $total = $this->consultarTotal();

        require_once "public/view/ventas.php";
    }

    public function mostrarClientes()
    {
        $clienteModelo = new Cliente();
        $clientes = $clienteModelo->ConsultarCliente();
        return $clientes;
    }
    public function mostrarProductos()
    {
        $productoModelo = new Producto();
        $productos = $productoModelo->mostrarProducto();
        return $productos;
    }

    public function mostrarCarrito()
    {
        $carritoModelo = new Carrito();
        $carrito = $carritoModelo->consultarCarrito();
        return $carrito;
    }
    public function consultarTotal()
    {
        $carritoModelo = new Carrito();
        $total = $carritoModelo->consultarTotal();
        return $total;
    }

    public function vaciarCarro()
    {
        $carritoModelo = new Carrito();
        $carrito = $carritoModelo->vaciarCarrito();
        return $carrito;
    }



    public function guardarCarrito()
    {
        $carritoModelo = new Carrito();
        $productoModelo = new Producto();
        $id_producto = $_POST['id_producto'];
        $cantidad = floatval($_POST['cantidad']);
        $valor_venta = floatval($_POST['valor_venta']);
        $subtotal = ($valor_venta * $cantidad);
        $stockDisponible = $productoModelo->consultarStock($id_producto);
        if (empty($id_producto) || empty($cantidad) || empty($subtotal) || empty($valor_venta)) {
            echo "<script>alert('Todos los campos son requeridos'); </script>";
            $this->mostrarVentas();
            return;
        }

        if ($cantidad <= $stockDisponible && $cantidad > 0 ) {
            $carritoModelo->insertarCarrito($id_producto, $cantidad, $valor_venta, $subtotal);
            $this->mostrarVentas();        
        } else {
            echo "<script>alert('El stock es insuficiente o ingresó un dato no valido')</script>";

            $this->mostrarVentas();
        }
        $this->mostrarVentas();
    }

    public function eliminarItemCarrito($id)
    {
        $carritoModelo = new Carrito();
        $carritoModelo->eliminarItem($id);
        $this->mostrarVentas();
    }

    public function procesarVenta()
    {
        $ventaModelo = new Venta();
        $productoModelo = new Producto();

        $id_cliente = $_POST['cliente_selecionado'];
        $valor_venta = $_POST['total_venta'];
        $carrito_temporal = $this->mostrarCarrito();
        if (
            empty($id_cliente) || empty($valor_venta)
        ) {
            echo "<script>alert('Todos los campos son requeridos')</script>";
            $this->mostrarVentas();
            return;
        }
        // se inserta la cabera y me retorna el id la cabecera "TABLA VENTA"
        $id_venta = $ventaModelo->insetarCabecera($id_cliente, $valor_venta);
        if ($id_venta !== false) {
            // Si la inserción de la cabecera fue exitosa, insertar los detalles de la venta
            foreach ($carrito_temporal as $item) {
                $id_producto = $item['id_producto'];
                $cantidad = $item['cantidad'];
                $subtotal = $item['subtotal'];
                $ventaModelo->insertarDetallesVentas($id_venta, $id_producto, $cantidad, $subtotal, $valor_venta);
                $productoModelo->descontarStock($id_producto, $cantidad);
            }

            echo "<script>alert('Venta realizada con extio')</script>";
            $this->vaciarCarro();
            $this->mostrarVentas();
        } else {
            echo "<script>alert('Hubo un error en procesar la venta')</script>";
            $this->vaciarCarro();
            $this->mostrarVentas();
        }
    }
}
