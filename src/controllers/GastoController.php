<?php

class GastoController
{

    public function __construct()
    {
        require_once 'src/models/Gasto.php';
        require_once 'src/models/Categoria.php';
    }

    public function mostrarGastos()
    {
        $GastoModelo = new Gasto();

        $Gastos = $GastoModelo->ConsultarGastos();
        $categorias = $this->mostrarCategorias();
        // // var_dump("hola", $categorias);
        // die;

        require_once "public/view/gastos.php";
    }

    public function GuardarGastos()
    {
        $GastoModelo = new Gasto();

        $nombre = $_POST['nombre'];
        $categoria_seleccionada = $_POST['categoria_seleccionada'];
        $valor = $_POST['valor'];

        if (empty($nombre) || empty($categoria_seleccionada) || empty($valor)) {
            echo "Todos los campos son obligatorios.";
            return;
        }

        $resultado = $GastoModelo->insertarGasto($nombre, $categoria_seleccionada, $valor);
        if ($resultado) {
            $this->mostrarGastos();

            echo "<script>alert('OK')</script>";
        } else {
            echo "<script>alert('ERROR')</script>";
        }
    }

    public function mostrarCategorias()
    {
        $CategoriaModelo = new Categoria();
        $categorias = $CategoriaModelo->ConsultarCategorias();
        return  $categorias;
    }

    public function FiltrarGastos()
    {
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $gastoModelo = new Gasto();
        $gastos = $gastoModelo->FiltroGasto($fecha_inicio, $fecha_fin);
        $sumatoria = array_sum(array_column($gastos, 'valor'));       
        require_once "public/view/filtroGastos.php";
    }



}
