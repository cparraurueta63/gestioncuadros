<?php

class ProductoController
{
    public function __construct()
    {
        require_once 'src/models/Producto.php';
        require_once 'src/models/Parametro.php';
    }



    public function mostrarProducto()
    {
        $productoModelo = new Producto();
        $productos = $productoModelo->mostrarProducto();
        $parametros = $this->mostrarParametros();
        require_once "public/view/producto.php";
    }

   
    public function mostrarParametros()
    {
        $parametrosModelo = new Parametro();
        $parametros = $parametrosModelo->mostrarParametros();
        return $parametros;
    }

    public function guardarProducto()
    {
        $productoModelo = new Producto();

        $nombre = $_POST['nombre'];
        $stock = $_POST['stock'];
        $alto = $_POST['alto'];
        $ancho = $_POST['ancho'];
        $valorNormal = $_POST['valorNormal'];
        $valorMinimo = $_POST['valorMinimo'];
        $valorIntermediario = $_POST['valorIntermediario'];
        $costoEstimado = $_POST['costoEstimado'];

        if (
            empty($nombre) || empty($valorNormal) || empty($stock) || empty($alto) || empty($ancho) ||
            empty($valorNormal) || empty($valorMinimo) || empty($valorIntermediario) || empty($costoEstimado)
        ) {
            echo "Todos los campos son obligatorios.";
            return;
        }

        $resultado = $productoModelo->insertarProductos($nombre, $stock, $alto, $ancho, $valorNormal, $valorMinimo, $valorIntermediario, $costoEstimado);
        if ($resultado) {
            $this->mostrarProducto();

            echo "<script>alert('OK')</script>";
        } else {
            echo "<script>alert('ERROR')</script>";
        }
    }
}
