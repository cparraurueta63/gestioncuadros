<?php

require_once('src/database/conexion.php');

class Categoria
{
    private $db;

    public function __construct()
    {
        $this->db = Conectar::conexion();
    }




    public function ConsultarCategorias()
    {
        $categorias = array();
        $query = "SELECT * FROM categoria_gastos";
        $ejecucion = $this->db->query($query);


        while ($fila = $ejecucion->fetch_assoc()) {
            $categorias[] = $fila;
        }

        // var_dump($categorias);
        return $categorias;

        // return false;
    }

}
