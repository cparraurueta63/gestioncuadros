<?php
require_once('src/database/conexion.php');

class Producto
{
    private $db;

    public function __construct()
    {
        $this->db = Conectar::conexion();
    }

    public function mostrarProducto()
    {

        $productos = array();
        $query = "SELECT * FROM productos";
        $ejecucion = $this->db->query($query);


        while ($fila = $ejecucion->fetch_assoc()) {
            $productos[] = $fila;
        }
        return $productos;
    }

    public function insertarProductos($nombre, $alto, $ancho, $stock, $valor_normal, $valor_minimo, $valor_intermediario, $estimado_costo)
    {
        $query = "INSERT INTO productos (nombre, alto, ancho, stock, valor_normal, valor_minimo, valor_intermediario, estimado_costo) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $ejecucion = $this->db->prepare($query);
        $ejecucion->bind_param("sddddddd", $nombre, $alto, $ancho, $stock, $valor_normal, $valor_minimo, $valor_intermediario, $estimado_costo);
        $resultado = $ejecucion->execute();
        $ejecucion->close();

        return $resultado;
    }

    public function consultarStock($id_producto)
    {
        $query = "SELECT stock FROM productos WHERE id = ?";
        $ejcucion = $this->db->prepare($query);
        $ejcucion->bind_param("d", $id_producto);
        $ejcucion->execute();
        $ejcucion->bind_result($stockActual);
        $ejcucion->fetch();
        $ejcucion->close();
        return $stockActual;
    }

    public function descontarStock($id_producto, $cantidad)
    {
        $stockActual = $this->consultarStock($id_producto);
        if ($stockActual >= $cantidad && $cantidad > 0) {

            $nuevoStock = $stockActual - $cantidad;
            $queryDescontar = "UPDATE productos SET stock = ? WHERE id = ?";
            $ejecucionDescontar = $this->db->prepare($queryDescontar);
            $ejecucionDescontar->bind_param("dd", $nuevoStock, $id_producto);
            $resultado = $ejecucionDescontar->execute();
            $ejecucionDescontar->close();
            return $resultado;
        } else {
            return false;
        }
    }
}
