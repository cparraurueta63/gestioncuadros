    <?php
    require_once('src/database/conexion.php');
    class Gasto
    {
        private $db;

        public function __construct()
        {
            $this->db = Conectar::conexion();
        }



        public function insertarGasto($nombre, $id_categoria, $valor)
        {
            $query = "INSERT INTO gastos (nombre, id_categoria, valor) VALUES (?, ?,?)";
            $ejecucion = $this->db->prepare($query);

            // Enlazamos los parámetros
            $ejecucion->bind_param('sdd', $nombre, $id_categoria, $valor);

            // Ejecutamos la consulta
            if ($ejecucion->execute()) {
                return true; // Inserción exitosa
            } else {
                return false; // Error en la inserción
                $ejecucion->close();
            }
        }

        public function ConsultarGastos()
        {
            $gastos = array();
            $query = "SELECT g.id, cg.nombre AS categoria, g.nombre AS gasto, g.valor, g.fecha_creacion
            FROM gastos g
            JOIN categoria_gastos cg ON g.id_categoria = cg.id;";
            $ejecucion = $this->db->query($query);


            while ($fila = $ejecucion->fetch_assoc()) {
                $gastos[] = $fila;
            }
            return $gastos;
        }

        public function FiltroGasto($fecha_inicio, $fecha_fin){
            $gastos = array();
            $query = "SELECT g.id, cg.nombre AS categoria, g.nombre AS gasto, g.valor, g.fecha_creacion
            FROM gastos g
            JOIN categoria_gastos cg ON g.id_categoria = cg.id
            WHERE g.fecha_creacion BETWEEN '$fecha_inicio 00:00:00' AND '$fecha_fin 23:59:59';
            ";
            
            $ejecucion = $this->db->query($query);
            while ($fila = $ejecucion->fetch_assoc()) {
                $gastos[] = $fila;
            }
            return $gastos;


        }
    }

    ?>