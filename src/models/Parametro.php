<?php

require_once('src/database/conexion.php');
class Parametro
{
    private $db;

    public function __construct()
    {
        $this->db = Conectar::conexion();
    }


    public function mostrarParametros(){

        $parametros = array();
        $query = "SELECT * FROM parametros";
        $ejecucion = $this->db->query($query);


        while ($fila = $ejecucion->fetch_assoc()) {
            $parametros[] = $fila;
        }
        return $parametros;
    

    }
}
