<?php

require_once('src/database/conexion.php');

class Carrito
{
    private $db;

    public function __construct()
    {
        $this->db = Conectar::conexion();
    }

    public function insertarCarrito($id_producto, $cantidad, $valor_venta, $subtotal)
    {
        // Verificar si el producto ya está en el carrito
        $query_verificacion = "SELECT id FROM carrito_temporal WHERE id_producto = ?";
        $verificacion = $this->db->prepare($query_verificacion);
        $verificacion->bind_param("d", $id_producto);
        $verificacion->execute();
        $verificacion->store_result();

        if ($verificacion->num_rows > 0) {

            return false;
        }


        $query = "INSERT INTO carrito_temporal (id_producto,cantidad, valor_venta, subtotal ) VALUES (?, ?, ?, ?)";
        $ejecucion = $this->db->prepare($query);


        $ejecucion->bind_param("dddd", $id_producto, $cantidad, $valor_venta, $subtotal);

        if ($ejecucion->execute()) {
            return true;
        } else {
            return false;
        }


        $ejecucion->close();
    }

    public function consultarCarrito()
    {
        $carrito = array();
        $query = "SELECT 
                        ct.id AS id_carrito,
                        p.nombre AS nombre_producto,
                        p.id AS id_producto,
                        ct.cantidad,
                        ct.valor_venta,
                        ct.subtotal,
                        pr.alto,
                        pr.ancho
                    FROM 
                        carrito_temporal ct
                    JOIN 
                        productos p ON ct.id_producto = p.id
                    JOIN 
                        productos pr ON ct.id_producto = pr.id;
    ";
        $ejecucion = $this->db->query($query);
        while ($fila = $ejecucion->fetch_assoc()) {
            $carrito[] = $fila;
        }
        return $carrito;
    }
    public function consultarTotal()
    {
        $total = 0;
        $query = "SELECT SUM(subtotal) AS Total FROM carrito_temporal;";
        $ejecucion = $this->db->query($query);
        if ($fila = $ejecucion->fetch_assoc()) {
            $total = $fila['Total'];
        }
        return $total;
    }

    public function eliminarItem($id)
    {
        $query = "DELETE FROM carrito_temporal WHERE id = ?";
        $ejecucion = $this->db->prepare($query);


        // Enlazamos los parámetros
        $ejecucion->bind_param("i", $id);

        // Ejecutamos la consulta
        if ($ejecucion->execute()) {
            return true; // Inserción exitosa
        } else {
            return false; // Error en la inserción
        }

        // Cerramos la declaración
        $ejecucion->close();
    }

    public function vaciarCarrito()
    {
        $query_delete = "DELETE FROM carrito_temporal";
        $query_alter = "ALTER TABLE carrito_temporal AUTO_INCREMENT = 1";

        // Preparar y ejecutar la consulta DELETE
        $ejecucion_delete = $this->db->prepare($query_delete);
        if (!$ejecucion_delete->execute()) {
            $ejecucion_delete->close();
            return false; // Si falla la ejecución, devuelve false
        }

        // Cerrar la declaración después de ejecutar la consulta DELETE
        $ejecucion_delete->close();

        // Preparar y ejecutar la consulta ALTER TABLE
        $ejecucion_alter = $this->db->prepare($query_alter);
        if (!$ejecucion_alter->execute()) {
            $ejecucion_alter->close();
            return false; // Si falla la ejecución, devuelve false
        }

        // Cerrar la declaración después de ejecutar la consulta ALTER TABLE
        $ejecucion_alter->close();

        return true; // Si ambas consultas se ejecutan con éxito, devuelve true
    }
}
