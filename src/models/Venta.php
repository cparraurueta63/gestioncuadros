<?php
require_once('src/database/conexion.php');

class Venta
{
    private $db;

    public function __construct()
    {
        $this->db = Conectar::conexion();
    }




    public function insetarCabecera($id_cliente, $valor_venta)
    {

        $query = "INSERT INTO ventas (id_cliente, valor_venta) VALUES (?, ?)";
        $ejecucion = $this->db->prepare($query);
        $ejecucion->bind_param("dd", $id_cliente, $valor_venta);

        if ($ejecucion->execute()) {
            $id_insertado = $this->db->insert_id;
            $ejecucion->close();
            return $id_insertado;
        } else {
            $ejecucion->close();
            return false;
        }
    }

    public function insertarDetallesVentas($id_venta, $id_producto, $cantidad, $subtotal, $total)
    {
        $query = "INSERT INTO detalles_venta (id_venta, id_producto, cantidad, subtotal, total ) VALUES (?, ?, ?, ?, ?)";
        $ejecucion = $this->db->prepare($query);
        $ejecucion->bind_param("ddddd", $id_venta, $id_producto, $cantidad, $subtotal, $total);
        if ($ejecucion->execute()) {
            $id_insertado = $this->db->insert_id;
            $ejecucion->close();
            return $id_insertado;
        } else {
            $ejecucion->close();
            return false;
        }
    }



}
