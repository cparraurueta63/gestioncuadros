<?php

require_once('src/database/conexion.php');

class Cliente
{
    private $db;

    public function __construct()
    {
        $this->db = Conectar::conexion();
    }



    public function insertarCliente($nombre, $celular, $direccion, $correo, $cedula)
    {
        $query = "INSERT INTO clientes (nombre, celular, direccion, correo, cedula) VALUES (?, ?, ?, ?, ?)";
        $ejecucion = $this->db->prepare($query);

        // Enlazamos los parámetros
        $ejecucion->bind_param("sssss", $nombre, $celular, $direccion, $correo, $cedula );

        // Ejecutamos la consulta
        if ($ejecucion->execute()) {
            return true; // Inserción exitosa
        } else {
            return false; // Error en la inserción
        }

        // Cerramos la declaración
        $ejecucion->close();
    }


    public function ConsultarCliente()
    {
        $clientes = array();
        $query = "SELECT * FROM clientes";
        $ejecucion = $this->db->query($query);


        while ($fila = $ejecucion->fetch_assoc()) {
            $clientes[] = $fila;
        }
        return $clientes;

        return false;
    }


    public function EliminarCliente($id)
    {
        $query = "DELETE FROM clientes WHERE id = ?";
        $ejecucion = $this->db->prepare($query);

        // Enlazamos los parámetros
        $ejecucion->bind_param("i", $id);

        // Ejecutamos la consulta
        if ($ejecucion->execute()) {
            return true; // Inserción exitosa
        } else {
            return false; // Error en la inserción
        }

        // Cerramos la declaración
        $ejecucion->close();
    }


    public function ConsultarClienteId($id)
    {

        $query = "SELECT * FROM clientes WHERE id= $id";
        $ejecucion = $this->db->query($query);
        $cliente = $ejecucion->fetch_assoc();
        return $cliente;
    }


    public function EditarCliente($id, $nombre, $celular, $direccion)
    {
        $query = "UPDATE clientes  SET nombre = ?, celular = ?, direccion =? WHERE id= ?";
        $ejecucion = $this->db->prepare($query);

        // Enlazamos los parámetros
        $ejecucion->bind_param($nombre, $celular, $direccion, $id);

        // Ejecutamos la consulta
        if ($ejecucion->execute()) {
            return true; // Inserción exitosa
        } else {
            return false; // Error en la inserción
        }

        // Cerramos la declaración
        $ejecucion->close();
    }
}
