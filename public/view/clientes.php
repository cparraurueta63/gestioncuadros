<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/view/css/style.css">
    <title>Clientes</title>
</head>

<body>
    <?php include('public/view/layout/header.php'); ?>

    <div class="contenedor">


        <div id="mensajes-error" class="error"></div>    

        <form action="index.php?c=cliente&a=GuardarCliente" method="post">
            <label for="cedula">Cedula:</label>
            <input type="text" id="cedula" name="cedula">

            <label for="nombre">Nombre:</label>
            <input type="text" id="nombre" name="nombre" required>

            <label for="telefono">Teléfono:</label>
            <input type="number" id="telefono" name="celular">

            <label for="direccion">Dirección:</label>
            <input type="text" id="direccion" name="direccion">

            <label for="correo">Correo:</label>
            <input type="text" id="correo" name="correo">

            <button  id="guardar" type="submit">Guardar Cliente</button>
        </form>


        <table class="tabla-clientes">
            <thead>
                <tr>
                    <th>Cedula</th>
                    <th>Nombre</th>
                    <th>Celular</th>
                    <th>Dirección</th>
                    <th>Correo</th>
                    <th>OP</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($clientes as $cliente) {
                    echo "<tr>";
                    echo "<td>" . $cliente['cedula'] . "</td>";
                    echo "<td>" . $cliente['nombre'] . "</td>";
                    echo "<td>" . $cliente['celular'] . "</td>";
                    echo "<td>" . $cliente['direccion'] . "</td>";
                    echo "<td>" . $cliente['correo'] . "</td>";
                    echo '<td><a href="index.php?c=cliente&a=VistaEliminarCliente&id=' . $cliente["id"] . '">Eliminar</a></td>';
                }
                ?>
            </tbody>
        </table>


    </div>

    <script src="public/view/js/validarFormCliente.js"></script>




</body>

</html>