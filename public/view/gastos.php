<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/view/css/style.css">
    <title>Gastos</title>
</head>

<body>
    <?php include('public/view/layout/header.php'); ?>
    <div class="contenedor">

        <div class="row">
            <div class="col-md-4">
                <form action="index.php?c=gasto&a=FiltrarGastos" method="post">


                    <label for="fecha_inicio">Fecha de Inicio:</label>
                    <input type="date" id="fecha_inicio" name="fecha_inicio" required>

                    <label for="fecha_fin">Fecha Fin:</label>
                    <input type="date" id="fecha_fin" name="fecha_fin" required>


                    <button type="submit">Filtrar</button>
                </form>
            </div>
            <div class="col-md-8">
                <form action="index.php?c=gasto&a=GuardarGastos" method="post">
                    <div class="row">
                        <div class="col-6"><label for="nombre">Titulo:</label>
                            <input type="text" id="nombre" name="nombre" required>
                        </div>
                        <div class="col-6"> <label for="valor">Valor:</label>
                            <input type="number" id="direccion" name="valor">
                        </div>
                    </div>
                    <label for="Categoria">Categoria:</label>
                    <select name="categoria_seleccionada">
                        <?php
                       
                        if (!empty($categorias)) {
                            foreach ($categorias as $categoria) {
                                echo "<option value=\"{$categoria['id']}\">{$categoria['nombre']}</option>";
                            }
                        } else {
                            echo "<option value=\"\">No hay categorías disponibles</option>";
                        }
                        ?>
                    </select>
                    <button type="submit">Guardar Gasto</button>
                </form>
            </div>
        </div>
        <table class="tabla-clientes">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Categoria</th>
                    <th>Valor</th>
                    <th>Fecha</th>
                    <!-- <th>OP</th> -->
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($Gastos as $gasto) {
                    echo "<tr>";
                    echo "<td>" . $gasto['gasto'] . "</td>";
                    echo "<td>" . $gasto['categoria'] . "</td>";
                    echo "<td>" . '$' . $gasto['valor'] . "</td>";
                    echo "<td>" . $gasto['fecha_creacion'] . "</td>";
                    // echo '<td><a href="index.php?c=cliente&a=VistaEliminarCliente&id=' . $cliente["id"] . '">Eliminar</a></td>';
                }
                ?>
            </tbody>
        </table>


    </div>
</body>

</html>