<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/view/css/style.css">
    <title>Ventas</title>
</head>

<body>
    <?php include('public/view/layout/header.php'); ?>

    <div class="contenedor">

        <div class="row">
            <div class="col-md-6">



                <table class="tabla-clientes">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Medidas</th>
                            <th>Stock</th>
                            <th>Referencias</th>
                            <th>Cantidad</th>
                            <th>Valor Venta</th>

                            <th>Opcion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        foreach ($productos as $producto) {
                            echo "<tr>";
                            echo "<td>" . $producto['nombre'] . "</td>";
                            echo "<td>Alto: " . $producto['alto'] . "<br>Ancho: " . $producto['ancho'] . "</td>";;
                            echo "<td>" . $producto['stock'] . "</td>";
                            echo "<td>Normal " . $producto['valor_normal'] . "<br>Intermedio " . $producto['valor_intermediario'] . "<br>Mínimo " . $producto['valor_minimo'] . "</td>";
                            echo '
                                <form class="form-carrito" action="index.php?c=venta&a=guardarCarrito" method="post">
                                    <td><input class="input-carrito cantidad"  name="cantidad"></td>
                                    <td><input class="input-carrito"  name="valor_venta"></td>
                                    <td>
                                        <input value="' . $producto['id'] . '" type="hidden" name="id_producto"> 
                                        <button class="btn-añadir-carrito" type="submit">+</button>
                                    </td>
                                </form>';
                            echo "</tr>";
                        }


                        ?>
                    </tbody>
                </table>
            </div>
            <!-- <div class="col-md-6">
                <?php echo "El valor total es " . $total ?>
                <table class="tabla-clientes">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Tamaño</th>
                            <th>Cantidad</th>
                            <th>Venta</th>
                            <th>Subtotal</th>
                            <th>Op</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($carrito as $i) {
                            echo "<tr>";
                            echo "<td>" . $i['nombre_producto'] . "</td>";
                            echo "<td>" . $i['alto'] . "<br>" . $i['ancho'] . "</td>";
                            echo "<td>" . $i['cantidad'] . "</td>";
                            echo "<td>" . $i['valor_venta'] . "</td>";
                            echo "<td>" . $i['subtotal'] . "</td>";
                            echo '<td><a href="index.php?c=venta&a=eliminarItemCarrito&id=' . $i["id_carrito"] . '">Eliminar</a></td>';
                        }
                        ?>
                    </tbody>
                </table>
            </div> -->


            <div class="col-md-6">
                <form action="index.php?c=venta&a=procesarVenta" method="post">

                    <label for="Clientes">Cliente para la venta:</label>
                    <select name="cliente_selecionado">
                        <option value="">Seleccione el cliente</option>
                        <?php foreach ($clientes as $cliente) { ?>

                            <option value="<?php echo $cliente['id']; ?>"><?php echo $cliente['nombre']; ?></option>
                        <?php } ?>
                    </select>


                    <table class="tabla-clientes">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Tamaño</th>
                                <th>Cantidad</th>
                                <th>Venta</th>
                                <th>Subtotal</th>
                                <th>Op</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($carrito as $i) {
                                echo "<tr>";
                                echo "<td>" . $i['nombre_producto'] . "</td>";
                                echo "<td>" . $i['alto'] . "<br>" . $i['ancho'] . "</td>";
                                echo "<td>" . $i['cantidad'] . "</td>";
                                echo "<td>" . $i['valor_venta'] . "</td>";
                                echo "<td>" . $i['subtotal'] . "</td>";
                                echo '<td><a href="index.php?c=venta&a=eliminarItemCarrito&id=' . $i["id_carrito"] . '">Eliminar</a></td>';
                            }
                            ?>
                        </tbody>
                    </table>
                    <label for="Clientes">El valor a pagar es:</label>
                    <input name="total_venta"  type="text" readonly value="<?php echo $total ?>">


                    <button type="submit">Procesar Venta</button>
                </form>
            </div>






        </div>
    </div>


</body>

</html>