document.addEventListener("DOMContentLoaded", function () {
  // Obtener referencias a los elementos del formulario
  var nombreInput = document.getElementById("nombre");
  var celularInput = document.getElementById("telefono");
  var direccionInput = document.getElementById("direccion");
  var correoInput = document.getElementById("correo");
  var cedulaInput = document.getElementById("cedula");

  // Agregar eventos de escucha para la entrada de los campos
  nombreInput.addEventListener("input", validarNombre);
  celularInput.addEventListener("input", validarCelular);
  direccionInput.addEventListener("input", validarDireccion);
  correoInput.addEventListener("input", validarCorreo);
  cedulaInput.addEventListener("input", validarCedula);
});

function deshabilitarBoton() {
  //   guardarClienteButton.setAttribute("disabled", "true");
  let guardarClienteButton = document.getElementById("guardar");

  guardarClienteButton.classList.add("desactivar-btn-cliente");
}

function habilitarBoton() {
  //   guardarClienteButton.removeAttribute("disabled");
  let guardarClienteButton = document.getElementById("guardar");
  guardarClienteButton.classList.remove("desactivar-btn-cliente");
}

function mostrarError(input, mensaje) {
  var mensajesError = document.getElementById("mensajes-error");
  mensajesError.textContent = mensaje;
  mensajesError.style.display = "block";
  deshabilitarBoton();
}

function ocultarError(input) {
  var mensajesError = document.getElementById("mensajes-error");
  mensajesError.textContent = "";
  mensajesError.style.display = "none";
  habilitarBoton();
}

function validarNombre() {
  var nombre = this.value;
  if (nombre.length > 50 || !isNaN(nombre)) {
    mostrarError(this, "Ingrese un nombre válido");
  } else {
    ocultarError(this);
  }
}

function validarCelular() {
  var celular = this.value;
  if (isNaN(celular) || celular.length != 10 || !/^3/.test(celular)) {
    mostrarError(
      this,
      "Ingrese un número de celular válido (10 dígitos que empiece por 3)"
    );
  } else {
    ocultarError(this);
  }
}

function validarDireccion() {
  var direccion = this.value;
  if (direccion.length > 100) {
    mostrarError(this, "La dirección no puede tener más de 100 caracteres");
  } else {
    ocultarError(this);
  }
}

function validarCorreo() {
  var correo = this.value;
  if (
    !/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test(correo) ||
    correo.length > 100
  ) {
    mostrarError(
      this,
      "Ingrese un correo electrónico válido (máximo 100 caracteres)"
    );
  } else {
    ocultarError(this);
  }
}

function validarCedula() {
  var cedula = this.value;
  if (isNaN(cedula) || cedula.length > 10) {
    mostrarError(
      this,
      "Ingrese una cédula válida (máximo 10 caracteres numéricos)"
    );
  } else {
    ocultarError(this);
  }
}
