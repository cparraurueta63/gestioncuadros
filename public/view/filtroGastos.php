<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/view/css/style.css">

    <title>Filtro Gastos</title>
</head>

<body>
    <?php include('public/view/layout/header.php'); ?>
    <div class="contenedor">
        <div class="tittle">
            <h3>El gasto total es: <?php echo '$' . number_format($sumatoria, 2); ?> </h3>
        </div>

        <table class="tabla-clientes">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Categoria</th>
                    <th>Valor</th>
                    <th>Fecha</th>
                    <!-- <th>OP</th> -->
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($gastos as $gasto) {
                    echo "<tr>";
                    echo "<td>" . $gasto['gasto'] . "</td>";
                    echo "<td>" . $gasto['categoria'] . "</td>";
                    echo "<td>" . '$' . $gasto['valor'] . "</td>";
                    echo "<td>" . $gasto['fecha_creacion'] . "</td>";
                    // echo '<td><a href="index.php?c=cliente&a=VistaEliminarCliente&id=' . $cliente["id"] . '">Eliminar</a></td>';
                }
                ?>
            </tbody>
        </table>
    </div>

</body>

</html>