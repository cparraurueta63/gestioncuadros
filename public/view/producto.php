<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="public/view/css/style.css">



    <title>Productos</title>
</head>

<body>



    <?php include('public/view/layout/header.php'); ?>
    <div class="contenedor">

        <form action="index.php?c=producto&a=guardarProducto" method="post">

            <div class="row">
                <div class="col-md-6">
                    <label for="nombre">Nombre:</label>
                    <input type="text" id="nombre" name="nombre" required>
                </div>
                <div class="col-md-6">
                    <label for="stock">Stock:</label>
                    <input type="number" id="stock" name="stock">
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="alto">Alto:</label>
                    <input type="number" id="alto" name="alto" onchange="calcularValores()">
                </div>
                <div class="col-md-6">
                    <label for="ancho">Ancho:</label>
                    <input type="number" id="ancho" name="ancho" onchange="calcularValores()">
                </div>
            </div>

            <!-- Nuevos campos -->
            <div class="row">
                <div class="col-md-6">
                    <label for="valorNormal">Valor:</label>
                    <input type="text" id="valorNormal" name="valorNormal" readonly>
                </div>
                <div class="col-md-6">
                    <label for="valorMinimo">Mínimo:</label>
                    <input type="text" id="valorMinimo" name="valorMinimo" readonly>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label for="valorIntermediario">Intermediario:</label>
                    <input type="text" id="valorIntermediario" name="valorIntermediario" readonly>
                </div>
                <div class="col-md-6">
                    <label for="costoEstimado">Costo:</label>
                    <input type="text" id="costoEstimado" name="costoEstimado" readonly>
                </div>
            </div>

            <button type="submit">Guardar </button>
        </form>

        <table class="tabla-clientes">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Alto</th>
                    <th>Ancho</th>
                    <th>Stock</th>
                    <th>V. Normal</th>
                    <th>V. Minimo</th>
                    <th>V. Intermedio</th>
                    <th>Costo Estimado</th>
                    <th>Fecha Creacion</th>
                    <th>Opcion</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($productos as $producto) {
                    echo "<tr>";
                    echo "<td>" . $producto['nombre'] . "</td>";
                    echo "<td>" . number_format($producto['alto'], 0, '', '') . "</td>";
                    echo "<td>" . number_format($producto['ancho'], 0, '', '') . "</td>";
                    echo "<td>" . number_format($producto['stock'], 0, '', '') . "</td>";
                    echo "<td>" . number_format($producto['valor_normal'], 0, '', '') . "</td>";
                    echo "<td>" . number_format($producto['valor_minimo'], 0, '', '') . "</td>";
                    echo "<td>" . number_format($producto['valor_intermediario'], 0, '', '') . "</td>";
                    echo "<td>" . number_format($producto['estimado_costo'], 0, '', '') . "</td>";
                    echo "<td>" . $producto['fecha_creacion'] . "</td>";
                    echo '<td><a href="index.php?c=producto&a=VistaVenderProducto&id=' . $producto["id"] . '">Vender</a></td>';
                }
                ?>
            </tbody>
        </table>


    </div>


</body>
<script>
    var parametros = <?php echo json_encode($parametros); ?>;
    console.log(parametros);


    function calcularValores() {
        var alto = document.getElementById('alto').value;
        var ancho = document.getElementById('ancho').value;


        var puntosCalculo = parametros.find(parametro => parametro.titulo === 'Puntos de cálculos cuadros').valor;


        var costoEstimado = puntosCalculo * alto * ancho;
        var valorNormal = costoEstimado * (1 + parametros[1].valor / 100);
        var valorMinimo = costoEstimado * (1 + parametros[2].valor / 100);
        var valorIntermediario = costoEstimado * (1 + parametros[3].valor / 100);


        document.getElementById('valorNormal').value = valorNormal.toFixed(0);
        document.getElementById('valorMinimo').value = valorMinimo.toFixed(0);
        document.getElementById('valorIntermediario').value = valorIntermediario.toFixed(0);
        document.getElementById('costoEstimado').value = costoEstimado.toFixed(0);
    }
</script>


</html>