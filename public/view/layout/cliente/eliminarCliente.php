<style>
    body {
        font-family: Arial, sans-serif;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        margin: 0;
    }

    .card {
        max-width: 300px;
        background-color: #fff;
        padding: 20px;
        border-radius: 8px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        text-align: center;
    }

    .button-container {
        display: flex;
        justify-content: space-between;
        margin-top: 20px;
    }

    .button {
        flex: 1;
        padding: 10px;
        margin: 0 5px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s;
    }

    .confirm-button {
        background-color: #4CAF50;
        color: #fff;
    }

    .cancel-button {
        background-color: #e74c3c;
        color: #fff;
    }

    .button:hover {
        background-color: #555;
    }
</style>
<div class="card">
    <h2>¿Estás seguro de eliminar a <?php echo $cliente['nombre']; ?> ?</h2>

    <div class="button-container">
        <form action="index.php?c=cliente&a=EliminarCliente" method="post">
            <input type="hidden" name="id" value="<?php echo $cliente['id']; ?>">
            <button class="button confirm-button" type="submit">Confirmar</button>
            <a class="button cancel-button" href="index.php?c=cliente&a=mostrarCliente">Cancelar</a>
        </form>
    </div>
</div>